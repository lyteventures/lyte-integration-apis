# Lyte Integration APIs

The Lyte Integration APIs is designed for closer integration between Lyte Partners' web portals and the Lyte applications, for the benefit of Partners' Users.

* [Terms of service](TOS.md)
* [Contact](mailto:api@lytecore.com)
* [Licence](LICENSE.md) (*updated 2019-06-07*)

## Contents

[TOC]

## Available API

### Base

Base specifications apply to all product-specific APIs.

Product | Version | Date
--- | --- | ---
[Base Specifications](base.md) | 1.0.0 | 31 May 2019

### LytePay

Product | Version | Date
--- | --- | ---
[LytePay for Real Estate](lytepay/real-estate.md) | 1.1.0 | 31 May 2019
