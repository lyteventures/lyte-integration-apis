# Lyte Integration APIs Documentation Licence

*Updated 2019-06-07*

By accessing, reading, or using this Documentation in any manner, You agree to be bound by the terms and conditions of this Licence. If You do not agree to the terms and conditions of this Licence, You must not use, read, or access the Documentation for any purpose whatsoever.

## 1. Definitions

**"Licence"** shall mean the terms and conditions for use, reproduction, and distribution as defined by Clauses 1 through 6 of this document.

**"Licensor"** (or **"We"**, **"Our"**, **"Ourselves"**, or **"Us"**) shall mean Lyte Ventures Pte Ltd (UEN: 201723769K), a company registered in Singapore, whose registered address is 6 Bukit Pasoh Road, #02-00, Singapore 089820, who is the copyright owner that is granting the Licence.

**"Legal Entity"** shall mean the union of the acting entity and all other entities that control, are controlled by, or are under common control with that entity. For the purposes of this definition, **"control"** means (i) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (ii) ownership of fifty percent (50%) or more of the outstanding shares, or (iii) beneficial ownership of such entity.

**"You"** (or **"Your"**) shall mean an individual or Legal Entity exercising permissions granted by this Licence.

**"Documentation"** shall mean the Lyte Integration APIs documentation, made available under this Licence, as amended from time to time.

## 2. Grant of Copyright Licence

Subject to the terms and conditions of this Licence, We hereby grant to You a non-exclusive, worldwide, non-transferable, non-sublicensable, royalty-free, and revocable copyright licence to use, review, store, print, translate the Documentation for Your own internal purposes in the course of Your ordinary business only. You shall not use the Documentation for any purpose other than as expressly permitted herein and if You wish to use the Documentation on or in connection with any products or services other than contemplated herein, You must be expressly authorised to do so in a separate licence agreement.

## 3. Trademarks

This Licence does not grant permission to use the trade names, trademarks, service marks, or product names of the Licensor, except as required for reasonable and customary use in describing the origin of the Documentation.

## 4. Disclaimer of Warranty

Unless required by applicable law or agreed to in writing, Licensor provides the Documentation on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied, including, without limitation, any warranties or conditions of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE. You are solely responsible for determining the appropriateness of using or redistributing the Documentation and assume any risks associated with Your exercise of permissions under this Licence.

## 5. Limitation of Liability

In no event and under no legal theory, whether in tort (including negligence), contract, or otherwise, unless required by applicable law (such as deliberate and grossly negligent acts) or agreed to in writing, shall We be liable to You for damages, including any direct, indirect, special, incidental, or consequential damages of any character arising as a result of this Licence or out of the use or inability to use the Documentation (including but not limited to damages for loss of goodwill, work stoppage, computer failure or malfunction, or any and all other commercial damages or losses), even if the Licensor has been advised of the possibility of such damages.

## 6. Indemnity

You shall indemnify Us and keep Us indemnified from and against any losses, damages, costs, or expenses suffered by Us and arising as a result of or in connection with any modification, alteration, addition made to the Document, or Your use, outside the scope of or not in accordance with this Licence. You further agree to indemnify, defend, and hold Us harmless for any liability incurred by, or claims asserted against, You or Us by reason of Your accepting and using this Licence.
