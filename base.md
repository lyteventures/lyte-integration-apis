# Lyte Integration APIs Base Specification

This document describes the base specification that applies to all APIs provided by Lyte.

## Contents

[TOC]

## Document Change History

Doc Version | Date | Author | Summary
--- | --- | --- | ---
1.0.0 | 31 May 2019 | Isaac Chua | Documentation release

## Server Endpoints

Lyte Integration APIs is provided over HTTP Secure (HTTPS) only. Partners and/or user-agents MUST ensure the server certificate provided upon making a connection is valid to prevent any man-in-the-middle attacks.

Name | URL
--- | ---
Production | https://api.lytecore.com/
Sandbox | https://sandbox.api.lytecore.com/

## Interpretation

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC 2119](https://tools.ietf.org/html/rfc2119).

All binary and Base64 data are presented in the Base 64 URL-safe encoding without padding characters, as defined in [RFC 4648 section 5](https://tools.ietf.org/html/rfc4648#section-5).

All sample JSONs have been formatted for human-readability. Minimising unnecessary whitespaces in production JSONs is RECOMMENDED.

### Terminology

Term | Definition
--- | ---
Lyte API JSON | Refers to the JSON object sent in a Lyte API HTTP response, which complies with [this specification](#markdown-header-lyte-api-json-response-specifications)
Lyte API JWT | Refers to the JSON Web Token (JWT) sent in a Lyte API HTTP request, which complies with [this specification](#markdown-header-lyte-api-json-web-token-jwt-specifications)
Partner | Refers to the partner of Lyte — i.e. you / the Partner
User | Refers to the user of the Lyte platform — i.e. the end-user

## API Design

The Lyte API is designed in accordance to the Representational State Transfer (REST) architectural style. The key difference between this API and most other REST-ful APIs is in the use of signed JSON Web Tokens (JWT) (more specifically, signed Lyte API JWTs) to make requests to the API server. In doing so:

* We abolish the use of API bearer keys for server authentication, which are otherwise sent in clear and carries a constant risk of loss, theft, and misuse.
* The Partner's proper generation and storage of the signing secret key will mean that the key never leaves their production server, thereby significantly reducing the risk of key theft and misuse.
* The use of JWTs enable novel implementation cases, such as those that do not require special network security configuration between the Partner and Lyte. This is done primarily through routing such JWTs through the end-User's browser.

Lyte provides means for Partners to manage and rotate public keys associated with their Lyte API account. Lyte will expire public keys after a period of time, in line with security best practices.

Responses sent by the Lyte API are typically in JSON format and not as JWT. A signed response JWT is not necessary as the signed request JWT and HTTPS connection are sufficient to mutually authenticate both parties. There is a possibility of replay attacks of previously signed JWTs, however, this is mitigated with the use of short expiry times built into the API's JWT validation.

## Lyte API Request Specifications

In this API, all request parameters for an API request SHALL be contained in a **Lyte API JWT**.

Such JSON Web Token (JWT) SHALL be signed with the Edwards-Curve Digital Signature Algorithm (EdDSA) and the Ed25519 curve.

### GET, DELETE Methods

All **GET** and **DELETE** requests SHALL contain one parameter only in the query string, whose value SHALL be the signed Lyte API JWT conveying all the parameters required for the request:

Name | Req? | Type | Description
--- | --- | --- | ---
`jwt` | Yes | Query | Signed JWT containing data of request

### POST, PUT, etc. Methods

As for **POST**, **PUT**, and other HTTP methods, the Lyte API JWT SHALL be placed in the request body.

## Lyte API JSON Web Token (JWT) Specifications

Per [RFC 7519](https://tools.ietf.org/html/rfc7519), digitally-signed JSON Web Tokens are represented using the JSON Web Signature (JWS) Compact Serialisation format, which comprises of three parts:

* A JWS Protected Header,
* A JWT Claims Set as the JWS Payload, and
* Its JWS Signature.

This section discusses Lyte API JWT requirements for each part.

### JWS Protected Header Requirements

The JWS Protected Headers in Lyte API JWTs MUST comply with the following specification:

Parameter | Req? | Name | Type/Value(s) | Description
--- | --- | --- | --- | ---
`"alg"` | Yes | Algorithm | `"EdDSA"` | Required per [RFC 8037](https://tools.ietf.org/html/rfc8037) to use EdDSA/Ed25519
`"typ"` | Yes | Type | `"lyte+jwt"` | Abridged media type of this JWT
`"kid"` | Yes | Key ID | `string` | JWK Thumbprint of key used to sign this JWT, in base64url

Because the JWS Protected Header does not change unless the key changes, it may be precomputed, encoded, and prepended on all JWTs until such time.

#### Sample JWS Protected Header

```
{
	"alg":"EdDSA",
	"typ":"lyte+jwt",
	"kid":"RI0PYpbSbfN3V-ieCfti-7CL9i0AX7aFuRvqMTuhR8M"
}
```

### JWT Claims Set Requirements

[RFC 7519](https://tools.ietf.org/html/rfc7519) defines several Registered Claim Names, and in that context, Lyte API JWTs MUST comply with the following specification:

Parameter | Req? | Name | Type/Value(s) | Description
--- | --- | --- | --- | ---
`"iss"` | Yes | Issuer | `string` (URL) | Partner portal URL; MUST minimally contain the scheme and authority
`"sub"` | Yes | Subject | `string` (email address) | End-user's email address
`"aud"` | Yes | Audience | `string` (URL) | Lyte server endpoint URL; MUST contain the scheme and authority, and MUST match desired endpoint
`"exp"` | - | Expiration Time | `number` (epoch time) | JWT will not be processed if `Current Time >= min("exp", "iat" + Lyte Token Max Expiry)`
`"nbf"` | - | Not Before | `number` (epoch time) | JWT will not be processed if `Current Time < "nbf"`
`"iat"` | Yes | Issued At | `number` (epoch time) | JWT will not be processed if `Current Time < "iat"`
`"jti"` | - | JWT ID | Any | Not used

Each API will define its specific Private Claim Names for its specific purposes.

#### Sample JWT Claims Set

```
{
	"iss":"https://www.example-agency.com.sg/",
	"sub":"johntan@example-agency.com.sg",
	"aud":"https://lytepay.co/",
	"iat":1547020869
}
```

### JWS Signature Requirements

All Lyte API JWTs must be digitally-signed with the Edwards-Curve Digital Signature Algorithm (EdDSA) and the Ed25519 curve. Its usage in JWS is defined in [RFC 8037](https://tools.ietf.org/html/rfc8037).

EdDSA/Ed25519 uses 256-bit keypairs, and generates 512-bit signatures. Encoded in Base 64 URL-safe, they are exactly 43 and 86 characters in length respectively.

The JWK Thumbprint of the keypair SHOULD be generated according to [RFC 7638](https://tools.ietf.org/html/rfc7638) and [RFC 8037](https://tools.ietf.org/html/rfc8037) and used in the JWS Protected Header.

EdDSA/Ed25519 keypairs can be generated and used to sign with the [libsodium](https://libsodium.gitbook.io/) library. There are bindings and ports for popular languages available.

Keeping secret keys encrypted at storage is RECOMMENDED.

#### EdDSA/Ed25519 Tool

We provide a browser-based EdDSA/Ed25519 tool that generates test keypairs and signed JWSes, and validates EdDSA/Ed25519-signed JWSes. This may be downloaded here:

https://bitbucket.org/lyteventures/ed25519-signature-tool

#### Sample EdDSA/Ed25519-signed JWT

Using the sample JWS Protected Header and JWT Claims Set above, and the sample keypair provided in this documentation, the following signed Lyte API JWT results:

```
eyJhbGciOiJFZERTQSIsInR5cCI6Imx5dGUrand0Iiwia2lkIjoiUkkwUFlwYlNiZk4zVi1pZUNmdGktN0NMOWkwQVg3YUZ1UnZxTVR1aFI4TSJ9.eyJpc3MiOiJodHRwczovL3d3dy5leGFtcGxlLWFnZW5jeS5jb20uc2cvIiwic3ViIjoiam9obnRhbkBleGFtcGxlLWFnZW5jeS5jb20uc2ciLCJhdWQiOiJodHRwczovL2x5dGVwYXkuY28vIiwiaWF0IjoxNTQ3MDIwODY5fQ.x9t7MnBB0cuVsbTKkx6l-4V3q45TOUPz1HmSOZXPTdgCttwhYPnShvDy8nr3ONvLBsjQbmHwGGb2jjMcJj_MBQ
```

## Lyte API JSON Response Specifications

All JSONs returned by Lyte APIs SHALL contain the following parameters:

Parameter | Req? | Name | Type/Value(s) | Description
--- | --- | --- | --- | ---
`"ver"` | Yes | Version | `string` | Version of the API endpoint
`"iss"` | Yes | Issuer | `string` (URL) | Lyte server endpoint URL; MUST minimally contain the scheme and authority
`"sub"` | Yes | Subject | `string` (email address) | End-user's email address
`"aud"` | Yes | Audience | `string` (URL) | Partner portal URL; MUST minimally contain the scheme and authority
`"iat"` | Yes | Issued At | `number` (epoch time) | When this JSON response was created

Each API will define its specific and additional JSON parameters for its specific purposes.

#### Sample JSON Response

```
{
	"ver":"1.0",
	"iss":"https://lytepay.co/",
	"sub":"johntan@example-agency.com.sg",
	"aud":"https://www.example-agency.com.sg/",
	"iat":1547022869
}
```

## EdDSA/Ed25519 Keypair Used in Samples

This documentation provides sample signed JWTs which use the following keys, in JSON Web Key (JWK) format. These keys MUST NOT be used in production.

#### Sample Secret Key

```
{
	"kty":"OKP",
	"crv":"Ed25519",
	"d":"-2RRzMFyUszISjR-RqKXeqB195BUc4vMJxWZVNtBSVY",
	"x":"JTE4I_0sZS8G48A20Yyi__Wwm9cNfSxfZbM9Fg7gBRg"
}
```

#### Sample Public Key

```
{
	"kty":"OKP",
	"crv":"Ed25519",
	"x":"JTE4I_0sZS8G48A20Yyi__Wwm9cNfSxfZbM9Fg7gBRg"
}
```

#### JWK Thumbprint of Sample Key

```
RI0PYpbSbfN3V-ieCfti-7CL9i0AX7aFuRvqMTuhR8M
```

## References

* [JSON Web Signature (JWS) [RFC 7515]](https://tools.ietf.org/html/rfc7515)
* [JSON Web Key (JWK) [RFC 7517]](https://tools.ietf.org/html/rfc7517)
* [JSON Web Token (JWT) [RFC 7519]](https://tools.ietf.org/html/rfc7519)
* [JSON Web Key (JWK) Thumbprint [RFC 7638]](https://tools.ietf.org/html/rfc7638)
* [Edwards-Curve Digital Signature Algorithm (EdDSA) [RFC 8032]](https://tools.ietf.org/html/rfc8032)
* [CFRG Elliptic Curve Diffie-Hellman (ECDH) and Signatures in JSON Object Signing and Encryption (JOSE) [RFC 8037]](https://tools.ietf.org/html/rfc8037)
* [JSON Web Token Best Current Practices (draft)](https://tools.ietf.org/html/draft-ietf-oauth-jwt-bcp-04)
