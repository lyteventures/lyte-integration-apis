# Terms of Service

Please contact your relationship manager or [legal@lyteventures.com](mailto:legal@lyteventures.com) for a copy of the Lyte Integration APIs terms of service. Your use of these APIs shall constitute your acceptance to the terms of service.
