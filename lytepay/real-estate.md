# LytePay for Real Estate Integration API

The LytePay for Real Estate Integration API is designed for closer integration between LytePay Partners' web portals and the LytePay for Real Estate application, for the benefit of Partners' Users — i.e. Real Estate Salespersons.

In this API version 1.1, Partners' web portals may be integrated with LytePay to directly submit Advance requests or to retrieve Advance status details.

## Contents

[TOC]

## Document Change History

Doc Version | API Version | Date | Author | Summary
--- | --- | --- | --- | ---
1.0.0 | 1.0 | 17 Jan 2019 | Isaac Chua | Documentation release for API version 1.0
1.0.1 | 1.0 | 23 Jan 2019 | Isaac Chua | Added Draft status to Advances; added guidance for undefined new statuses; changed Advance submission API method from POST to GET
1.0.2 | 1.0 | 28 Jan 2019 | Isaac Chua | Added Withdrawn status to Advances
1.1.0 | 1.1 | 31 May 2019 | Isaac Chua | Updated documentation to use API gateway

## Server Endpoints

Name | URL
--- | ---
Production | https://api.lytecore.com/lpre/1.1
Sandbox | https://sandbox.api.lytecore.com/lpre/1.1

## Interpretation

This documentation MUST be read together with the Base specifications.

# Advances

## **GET** `/advances/getStatus` — Retrieve statuses of advances

Given an array of Transaction IDs, this API retrieves the statuses of Advances on these Transactions.

There are 13 Advance statuses on LytePay. Some of these statuses may be deemed as "Requestable", which means a new Advance request may be submitted to LytePay, subject to approvals.

Name | Value | Description | Requestable?
--- | --- | --- | ---
No Status / New | `"ns"` | This Transaction ID does not exist on LytePay | Yes
Draft | `"dr"` | Advance request is in draft state | Yes
Pending Approval | `"ap"` | Advance request is pending Partner approval | -
Withdrawn | `"wi"` | Advance request was withdrawn by User while pending Partner approval | Yes
Rejected | `"re"` | Advance request has been rejected by Partner | Yes
Pending Acceptance | `"ac"` | Advance offer is pending User's acceptance | -
Declined | `"de"` | Advance offer was declined by User | Yes
Expired | `"ex"` | Advance offer has expired | Yes
Processing | `"pr"` | LytePay is processing payment to User | -
Paid | `"pa"` | LytePay has paid User | -
Cancelled | `"ca"` | LytePay has cancelled the Advance | Yes
Blocked | `"bl"` | LytePay has blocked Advances on this Transaction | -
Error | `"er"` | There was an error while processing this Transaction ID | -

In the likely case of new statuses not defined in this version of this document, they should be treated the same way as "No Status / New", including that such transaction is requestable.

### Request

#### JWT Private Claims Model

Parameter | Req? | Name | Type/Value(s) | Description
--- | --- | --- | --- | ---
`"ids"` | Yes | Transaction IDs | `array[string]` | Array of Transaction IDs to retrieve Advance statuses of

Note that there is a maximum cap of 50 Transaction IDs in a single API call. IDs beyond the cap will not be processed. For increases to this limit, please contact your relationship manager.

#### Sample Request

##### JWT Claims Set
```
{
	"iss":"https://www.example-agency.com.sg/",
	"sub":"johntan@example-agency.com.sg",
	"aud":"https://lytepay.co/",
	"iat":1547020869,
	"ids":["11111","22222","33333",
		"44444","55555","66666","77777",
		"88888","99999","00000","abcde"]
}
```

##### API Call with Signed JWT
```
GET /lpre/1.1/advances/getStatus?jwt=eyJhbGciOiJFZERTQSIsInR5cCI6Imx5dGUrand0Iiwia2lkIjoiUkkwUFlwYlNiZk4zVi1pZUNmdGktN0NMOWkwQVg3YUZ1UnZxTVR1aFI4TSJ9.eyJpc3MiOiJodHRwczovL3d3dy5leGFtcGxlLWFnZW5jeS5jb20uc2cvIiwic3ViIjoiam9obnRhbkBleGFtcGxlLWFnZW5jeS5jb20uc2ciLCJhdWQiOiJodHRwczovL2x5dGVwYXkuY28vIiwiaWF0IjoxNTQ3MDIwODY5LCJpZHMiOlsiMTExMTEiLCIyMjIyMiIsIjMzMzMzIiwiNDQ0NDQiLCI1NTU1NSIsIjY2NjY2IiwiNzc3NzciLCI4ODg4OCIsIjk5OTk5IiwiMDAwMDAiLCJhYmNkZSJdfQ.0t7FUfFg1-U1Z6gBDshLFUrK2Zn3H20-QKh7zs_XlfJfqsgKUaHBnWTt060y5DLUKBRp9ty6smmbl7Ei6mTSDQ HTTP/1.1
Host: api.lytecore.com
```

### Responses

#### Response Codes

Code | Name | Description
--- | --- | ---
200 | OK | Operation completed successfully, `application/json` response included in body
400 | Bad Request | When submitted JWT is malformed, expired, or "not before"; supplied parameters are not valid or allowed; or required parameters are blank
405 | Method Not Allowed | When a method other than **GET** is used
406 | Not Acceptable | When `Accept` request header does not include `application/json`
414 | URI Too Long | When the request URI is too long, such as if the JWT is too large
415 | Unsupported Media Type | When `"typ"` in the JWS Protected Header is not `lyte+jwt`
429 | Too Many Requests | When number of requests have exceeded the limit in a given time

#### JSON Response Model

Parameter | Req? | Name | Type/Value(s) | Description
--- | --- | --- | --- | ---
`"advances"` | Yes | Advances | `array[Advance]` | Array of Advance objects containing status details of submitted Transaction IDs

#### `Advance` JSON Model

Parameter | Req? | Name | Type/Value(s) | Description
--- | --- | --- | --- | ---
`"id"` | Yes | Transaction ID | `string` | Transaction ID of the Advance
`"status"` | Yes | Status | `"ns"`, `"ap"`, `"re"`, `"ac"`, `"de"`, `"ex"`, `"pr"`, `"pa"`, `"ca"`, `"bl"`, `"er"` | Status of the Advance defined above

#### Sample Response

```
{
	"ver":"1.1",
	"iss":"https://lytepay.co/",
	"sub":"johntan@example-agency.com.sg",
	"aud":"https://www.example-agency.com.sg/",
	"iat":1547022869,
	"advances":[
		{"id":"11111","status":"ns"},
		{"id":"22222","status":"ap"},
		{"id":"33333","status":"re"},
		{"id":"44444","status":"ac"},
		{"id":"55555","status":"de"},
		{"id":"66666","status":"ex"},
		{"id":"77777","status":"pr"},
		{"id":"88888","status":"pa"},
		{"id":"99999","status":"ca"},
		{"id":"00000","status":"bl"},
		{"id":"abcde","status":"er"}
	]
}
```

## **GET** `/advances/submit` — Submits an Advance request

This begins an Advance request for a User, with the details of the request given through the API.

### Request

#### JWT Private Claims Model

Parameter | Req? | Name | Type/Value(s) | Description
--- | --- | --- | --- | ---
`"id"` | Yes | Transaction ID | `string` | Transaction ID of the Advance request
`"development"` | Yes | Development | `string` | Name of development where the unit was sold
`"address"` | Yes | Address of Unit | `string` | Address of the unit sold
`"otp_exercise_date"` | Yes | OTP Exercise Date | `string` (ISO 8601) | Local date of the OTP exercise date
`"net_commission"` | Yes | Net Commission | `number` | Net commission amount in local currency
`"associate_number"` | Yes | Associate Number | `string` | User's associate number
`"name"` | Yes | User's Full Name | `string` | User's full name
`"email"` | - | User's Email | `string` (email address) | User's email address; MUST match `"sub"` if used
`"phone_number"` | Yes | User's Mobile Number | `string` (E.164) | User's mobile number, without spaces or dashes
`"cea_number"` | Yes | User's CEA Number | `string` | User's CEA number

#### Sample Request

##### JWT Claims Set
```
{
	"iss":"https://www.example-agency.com.sg/",
	"sub":"johntan@example-agency.com.sg",
	"aud":"https://lytepay.co/",
	"iat":1547020869,
	"id":"abc12345",
	"development":"PARC ESTA",
	"address":"836 SIMS AVENUE #02-04 PARC ESTA SINGAPORE 400836",
	"otp_exercise_date":"2018-10-03",
	"net_commission":12000.00,
	"associate_number":"K12345",
	"name":"John Tan",
	"phone_number":"+6591234567",
	"cea_number":"R123456A"
}
```

##### API Call with Signed JWT
```
GET /lpre/1.1/advances/submit?jwt=eyJhbGciOiJFZERTQSIsInR5cCI6Imx5dGUrand0Iiwia2lkIjoiUkkwUFlwYlNiZk4zVi1pZUNmdGktN0NMOWkwQVg3YUZ1UnZxTVR1aFI4TSJ9.eyJpc3MiOiJodHRwczovL3d3dy5leGFtcGxlLWFnZW5jeS5jb20uc2cvIiwic3ViIjoiam9obnRhbkBleGFtcGxlLWFnZW5jeS5jb20uc2ciLCJhdWQiOiJodHRwczovL2x5dGVwYXkuY28vIiwiaWF0IjoxNTQ3MDIwODY5LCJpZCI6ImFiYzEyMzQ1IiwiZGV2ZWxvcG1lbnQiOiJQQVJDIEVTVEEiLCJhZGRyZXNzIjoiODM2IFNJTVMgQVZFTlVFICMwMi0wNCBQQVJDIEVTVEEgU0lOR0FQT1JFIDQwMDgzNiIsIm90cF9leGVyY2lzZV9kYXRlIjoiMjAxOC0xMC0wMyIsIm5ldF9jb21taXNzaW9uIjoxMjAwMC4wMCwiYXNzb2NpYXRlX251bWJlciI6IksxMjM0NSIsIm5hbWUiOiJKb2huIFRhbiIsInBob25lX251bWJlciI6Iis2NTkxMjM0NTY3IiwiY2VhX251bWJlciI6IlIxMjM0NTZBIn0.gV7kiM8Ne49boQKCcn0h1mVpe87WO96wddlQMbiA6x35JslWW3KPKX54ja9AZ0wbEhWivaGOFZllrki016WBBQ HTTP/1.1
Host: api.lytecore.com
```

### Responses

#### Response Codes

Code | Name | Description
--- | --- | ---
200 | OK | Operation completed successfully, Advance request page shown to User
400 | Bad Request | When submitted JWT is malformed, expired, or "not before"; supplied parameters are not valid or allowed; or required parameters are blank
405 | Method Not Allowed | When a method other than **GET** are used
406 | Not Acceptable | When `Accept` request header does not include `text/html`
414 | URI Too Long | When the request URI is too long, such as if the JWT is too large
415 | Unsupported Media Type | When `"typ"` in the JWS Protected Header is not `lyte+jwt`
429 | Too Many Requests | When number of requests have exceeded the limit in a given time
